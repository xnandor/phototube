//c++ libraries
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <vector>

//c libraries
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include <cstdlib>

#define PI 3.14159

namespace Phototube {
  class Point {
  public:
    double x, y, z;
    Point(double, double, double);
  };

  Point::Point(double newX, double newY, double newZ) {
    x = newX;
    y = newY;
    z = newZ;
  }
}

namespace Phototube {
  class Index {
  public:
    int one, two, three;
    Index(int, int, int);
  };
  
  Index::Index(int newOne, int newTwo, int newThree) {
      one   = newOne;
      two   = newTwo;
      three = newThree;
  }
}

namespace Phototube {
  class Viewer {
  private:
  public:
    static float time;
    static GLfloat zoom;
    static GLfloat zoomVelocity;
    static GLfloat zoomAcceleration;
    static GLfloat translation[3];
    static GLfloat translationalVelocity[3];
    static GLfloat translationalAcceleration[3];
    static GLfloat rotation[3];
    static GLfloat rotationalVelocity[3];
    static GLfloat rotationalAcceleration[3];
    
    static GLboolean isLighting;
    static GLboolean isSphere;
    static GLboolean isCoordinate;
    static GLboolean isFile;
    static GLboolean isInfo;


    //aR, aG, aB, dR, dG, dB, sR, sG, sB, Shininess
    static int lightingSelector;
    static GLfloat lighting[10];

    static std::vector< Phototube::Point > vertices;
    static std::vector< Phototube::Index > faces;

    static void display();
    static void update();
    static void initCamera();
    static void initGlut(int argc, char** argv);
    static void initGL();
    static void keyboard(unsigned char key, int x, int y);
    static void keyboardSpecial(int key, int x, int y);
    static void reshape(int w, int h);
    static void readFile(int argc, char** argv);
    static void drawCoordinate();
    static void drawFile();
    static void drawInfo();
  };

}


float Phototube::Viewer::time = 0;

GLfloat Phototube::Viewer::zoom = 20.0;
GLfloat Phototube::Viewer::zoomVelocity = 0.0;
GLfloat Phototube::Viewer::zoomAcceleration = 0.0;

GLfloat Phototube::Viewer::rotation[3] = {0.0, 0.0, 0.0};
GLfloat Phototube::Viewer::rotationalVelocity[3] = {0.0, 0.0, 0.0};
GLfloat Phototube::Viewer::rotationalAcceleration[3] = {0.0, 0.0, 0.0};

GLfloat Phototube::Viewer::translation[3] = {0.0, -0.1, 0.0};
GLfloat Phototube::Viewer::translationalVelocity[3] = {0.0, 0.0, 0.0};
GLfloat Phototube::Viewer::translationalAcceleration[3] = {0.0, 0.0, 0.0};

GLboolean Phototube::Viewer::isLighting = true;
GLboolean Phototube::Viewer::isCoordinate = false;
GLboolean Phototube::Viewer::isFile = true;
GLboolean Phototube::Viewer::isInfo = true;

int Phototube::Viewer::lightingSelector = 0;
GLfloat Phototube::Viewer::lighting[10] = {0.1, 0.1, 0.1, 0.8, 0.8, 0.8, 0.5, 0.5, 0.5, 30};

std::vector< Phototube::Point > Phototube::Viewer::vertices;
std::vector< Phototube::Index > Phototube::Viewer::faces;


void Phototube::Viewer::keyboard (unsigned char key, int x, int y) {
  switch (key) {
    //Camera Movement
  case '-': zoomAcceleration             -= 0.0005; break;
  case '=': zoomAcceleration             += 0.0005; break;
  case 'a': translationalAcceleration[0] -= 0.00005; break;
  case 'd': translationalAcceleration[0] += 0.00005; break;
  case 's': translationalAcceleration[1] -= 0.00005; break;
  case 'w': translationalAcceleration[1] += 0.00005; break;
  case 'q': rotationalAcceleration[0]    -= 0.2; break;
  case 'e': rotationalAcceleration[0]    += 0.2; break;
  case 'f': rotationalAcceleration[1]    -= 0.2; break;
  case 'r': rotationalAcceleration[1]    += 0.2; break;
    //Toggles
  case 'l': isLighting   = !isLighting; break;
  case 'u': isCoordinate = !isCoordinate; break;
  case 'o': isFile       = !isFile; break;
  case 'i': isInfo       = !isInfo; break;
  
  default: break;
  }
  glutPostRedisplay();
}

//This method handles all user interaction with the HUD
void Phototube::Viewer::keyboardSpecial (int key, int x, int y) {
  if (isInfo) {
    switch (key) {
      //Camera Movement
    case GLUT_KEY_UP: 
      lightingSelector--;
      if (lightingSelector < 0) lightingSelector = 12;
      break;

    case GLUT_KEY_DOWN: 
      lightingSelector++;
      if (lightingSelector > 12) lightingSelector = 0;
      break;

    case GLUT_KEY_LEFT:
      if (lightingSelector < 9) {
	lighting[lightingSelector] -= 0.01;
      } 
      if (lightingSelector == 9) {
	lighting[lightingSelector] -= 1.0;
      }
      if (lightingSelector == 10) {
	lighting[0] -= 0.01;
	lighting[1] -= 0.01;
	lighting[2] -= 0.01;
      }
      if (lightingSelector == 11) {
	lighting[3] -= 0.01;
	lighting[4] -= 0.01;
	lighting[5] -= 0.01;
      }
      if (lightingSelector == 12) {
	lighting[6] -= 0.01;
	lighting[7] -= 0.01;
	lighting[8] -= 0.01;
      }
      break;
      
    case GLUT_KEY_RIGHT:
      if (lightingSelector < 9) {
	lighting[lightingSelector] += 0.01;
      } 
      if (lightingSelector == 9) {
	lighting[lightingSelector] += 1.0;
      }
      if (lightingSelector == 10) {
	lighting[0] += 0.01;
	lighting[1] += 0.01;
	lighting[2] += 0.01;
      }
      if (lightingSelector == 11) {
	lighting[3] += 0.01;
	lighting[4] += 0.01;
	lighting[5] += 0.01;
      }
      if (lightingSelector == 12) {
	lighting[6] += 0.01;
	lighting[7] += 0.01;
	lighting[8] += 0.01;
      }
      break;
      
    default: break;
    }
  }
}

// Should only be used for physics simulation
void Phototube::Viewer::update() {
  time +=1;

  zoomVelocity += zoomAcceleration;
  zoom += zoomVelocity;
  zoomAcceleration = 0;
  zoomVelocity /= 1.005;

  translationalVelocity[0] += translationalAcceleration[0];
  translation[0] += translationalVelocity[0];
  translationalAcceleration[0] = 0;
  translationalVelocity[0] /= 1.002;

  translationalVelocity[1] += translationalAcceleration[1];
  translation[1] += translationalVelocity[1];
  translationalAcceleration[1] = 0;
  translationalVelocity[1] /= 1.002;

  rotationalVelocity[0] += rotationalAcceleration[0];
  rotation[0] += rotationalVelocity[0];
  rotationalAcceleration[0] = 0;
  rotationalVelocity[0] /= 1.01;

  rotationalVelocity[1] += rotationalAcceleration[1];
  rotation[1] += rotationalVelocity[1];
  rotationalAcceleration[1] = 0;
  rotationalVelocity[1] /= 1.01;

  display();
}


// Primary display loop from which all other drawing happens.
void Phototube::Viewer::display(void) {
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  glPushMatrix();
  glScalef(zoom, zoom, zoom);
  glTranslatef(translation[0], translation[1], translation[2]);
  glRotatef(rotation[0], 0.0, 1.0, 0.0);
  glRotatef(rotation[1], -1.0, 0.0, 0.0);
  glRotatef(rotation[2], 0.0, 0.0, 1.0);

  if (isLighting) {
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    GLfloat ambient[4]  = {1.0, 1.0, 1.0, 1.0};
    GLfloat diffuse[4]  = {1.0, 1.0, 1.0, 1.0};
    GLfloat specular[4] = {1.0, 1.0, 1.0, 1.0};
    GLfloat position[3] = {2, 3, 2};
    glLightfv(GL_LIGHT0, GL_AMBIENT,  &ambient[0]);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  &diffuse[0]);
    glLightfv(GL_LIGHT0, GL_SPECULAR, &specular[0]);
    glLightfv(GL_LIGHT0, GL_POSITION, &position[0]);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  } else {
    glDisable(GL_LIGHTING);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glColor3f(1,0,0);
  }

  if (isFile) {
    Phototube::Viewer::drawFile();
  }

  if (isCoordinate) {
    Phototube::Viewer::drawCoordinate();
  }

  glPopMatrix();

  if (isInfo) {
    Phototube::Viewer::drawInfo();
  }

  glFlush();
  glutSwapBuffers();
}

//Draws a unit grid on the xz plane
//   is also a toggle
void Phototube::Viewer::drawCoordinate() {
  glDisable(GL_LIGHTING);
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  glColor3f(0.9, 0.9, 0.9);
  glBegin(GL_TRIANGLES);
  GLfloat step = 0.1;
  for (GLfloat x = -1.0; x <= 1.0; x += step) {
    for (GLfloat z = -1.0; z < 1.0; z += step) {
      GLfloat v1[3] = {x,      0.0, z,    };
      GLfloat v2[3] = {x+step, 0.0, z,    };
      GLfloat v3[3] = {x,      0.0, z+step};
      GLfloat v4[3] = {x+step, 0.0, z+step};
      glVertex3fv(&v1[0]);
      glVertex3fv(&v4[0]);
      glVertex3fv(&v2[0]);
      glVertex3fv(&v1[0]);
      glVertex3fv(&v3[0]);
      glVertex3fv(&v4[0]);
    }
  }
  
  glEnd();
}


// This method Draws the input file while computing its normals
//Normals are computed per face while considering  the objects zoom amount
void Phototube::Viewer::drawFile() {
  glBegin(GL_TRIANGLES);
  glShadeModel(GL_SMOOTH);
  GLfloat ambient[4] = {lighting[0], lighting[1], lighting[2], 1.0};
  GLfloat diffuse[4] = {lighting[3], lighting[4], lighting[5], 1.0};
  GLfloat specular[4] = {lighting[6], lighting[7], lighting[8], 1.0};
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT,   &ambient[0]);
  glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE,   &diffuse[0]);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR,  &specular[0]);
  glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, lighting[9]);
  for (unsigned int i = 0; i < faces.size(); i++) {
    Phototube::Index index = faces[i];
    GLfloat v1[3];
    Phototube::Point point1 = vertices[index.one];
    v1[0] = point1.x;
    v1[1] = point1.y;
    v1[2] = point1.z;
    GLfloat v2[3];
    Phototube::Point point2 = vertices[index.two];
    v2[0] = point2.x;
    v2[1] = point2.y;
    v2[2] = point2.z;
    GLfloat v3[3];
    Phototube::Point point3 = vertices[index.three];
    v3[0] = point3.x;
    v3[1] = point3.y;
    v3[2] = point3.z;
    //make normal from vectors a and b which represent v1-->v2 and v1-->v3.
    GLfloat a[3] = {v1[0]-v2[0], v1[1]-v2[1], v1[2]-v2[2] };
    GLfloat b[3] = {v1[0]-v3[0], v1[1]-v3[1], v1[2]-v3[2] };
    GLfloat normal[4] = {0.0, 0.0, 0.0, 1.0};
    normal[0] = (a[1]*b[2])-(a[2]*b[1]);
    normal[1] = -((a[0]*b[2])-(a[2]*b[0]));
    normal[2] = (a[0]*b[1])-(a[1]*b[0]);
    //make unit vector
    GLfloat length = sqrt( pow(normal[0],2) + pow(normal[1],2) + pow(normal[2],2) );
    normal[0] = (normal[0]/length)*zoom;
    normal[1] = (normal[1]/length)*zoom;
    normal[2] = (normal[2]/length)*zoom;
    glNormal3fv(&normal[0]);
    glVertex3fv(&v1[0]);
    glVertex3fv(&v2[0]);
    glVertex3fv(&v3[0]);
  }
  glEnd(); 
}


//  This method draws the shading info on the screen 
//  with user interaction from the keyboard arrows
//  it is called when the information toggle is on.
void Phototube::Viewer::drawInfo() {
  glPushMatrix();
  glTranslatef(0.0, 0.0, 99.0);
  glDisable(GL_LIGHTING);

  char string[26][80];
  std::string dummy;
  std::string stringLine;
  std::stringstream ss;

  ss << "ambient R:" << lighting[0]; 
  stringLine = ss.str();  ss.str("");
  strcpy(string[0], stringLine.c_str());
  ss << "ambient G:" << lighting[1]; 
  stringLine = ss.str();  ss.str("");
  strcpy(string[1], stringLine.c_str());
  ss << "ambient B:" << lighting[2]; 
  stringLine = ss.str();  ss.str("");
  strcpy(string[2], stringLine.c_str());
  ss << "diffuse R:" << lighting[3]; 
  stringLine = ss.str();  ss.str("");
  strcpy(string[3], stringLine.c_str());
  ss << "diffuse G:" << lighting[4]; 
  stringLine = ss.str();  ss.str("");
  strcpy(string[4], stringLine.c_str());
  ss << "diffuse B:" << lighting[5]; 
  stringLine = ss.str();  ss.str("");
  strcpy(string[5], stringLine.c_str());
  ss << "specular R:" << lighting[6]; 
  stringLine = ss.str();  ss.str("");
  strcpy(string[6], stringLine.c_str());
  ss << "specular G:" << lighting[7]; 
  stringLine = ss.str();  ss.str("");
  strcpy(string[7], stringLine.c_str());
  ss << "specualr B:" << lighting[8]; 
  stringLine = ss.str();  ss.str("");
  strcpy(string[8], stringLine.c_str());
  ss << "Shininess:" << lighting[9];
  stringLine = ss.str();  ss.str("");
  strcpy(string[9], stringLine.c_str());

  int lines = 10;
  double y = 1.85;
  for (int line = 0; line < lines; line++) {
    //draw highlighted line
    if (line == lightingSelector) {
      glColor3f(1.0, 0.8, 0.0);
    } else {
      glColor3f(1.0, 0.0, 0.0);
    }
    if ( (line == 0 || line == 1 || line == 2) && (lightingSelector == 10) ) {
      glColor3f(1.0, 0.8, 0.0);
    }
    if ( (line == 3 || line == 4 || line == 5) && (lightingSelector == 11) ) {
      glColor3f(1.0, 0.8, 0.0);
    }
    if ( (line == 6 || line == 7 || line == 8) && (lightingSelector == 12) ) {
      glColor3f(1.0, 0.8, 0.0);
    }

    //setup text position;
    glRasterPos2f(-3.5, y);
    y -= 0.15;
    int characters = (int) strlen(string[line]);
    //Draw Line of Text
    for (int character = 0; character < characters; character++) {
      glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, string[line][character]);
    }

  }
  glPopMatrix();

}

void Phototube::Viewer::reshape(int width, int height)
{
  glViewport(0, 0, width, height);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  if (width <= height) {
    glOrtho(-2.0, 2.0, -2.0*(GLfloat)height/(GLfloat)width, 2.0*(GLfloat)height/(GLfloat)width,-600, 600);
  } else { 
    glOrtho(-2.0*(GLfloat)width/(GLfloat)height, 2.0*(GLfloat)width/(GLfloat)height, -2.0,2.0, -600, 600); 
  }
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void Phototube::Viewer::initGlut(int argc, char** argv) {
  std::cout << "Viewer Initiated \n";
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(1280,720);
  glutCreateWindow(argv[0]);
  Phototube::Viewer::initGL();
  glutDisplayFunc(Phototube::Viewer::display);
  glutReshapeFunc(Phototube::Viewer::reshape);
  glutKeyboardFunc(Phototube::Viewer::keyboard);
  glutSpecialFunc(Phototube::Viewer::keyboardSpecial);
  glutIdleFunc(Phototube::Viewer::update);
  glutMainLoop();
}

void Phototube::Viewer::initGL() {
  glEnable(GL_DEPTH_TEST);  
  glClearColor(1.0, 1.0, 1.0, 1.0);
  glEnable(GL_LINE_SMOOTH);
  glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
  glEnable(GL_POINT_SMOOTH);
  glHint(GL_POINT_SMOOTH_HINT, GL_NICEST); 
  glPointSize(3.0);
}


// TODO: readFile needs to be expanded to read all
// data in a file. Normals should be added soon.
void Phototube::Viewer::readFile(int argc, char** argv) {
  std::string line;
  std::ifstream infile;
  infile.open(argv[argc-1]);
  while (std::getline(infile, line)) {
    std::stringstream ss(line);
    std::string word;
    ss >> word;
    if (word == "v") {
      double x, y, z;
      ss >> x >> y >> z;
      vertices.push_back( Point(x,y,z) );
    } else if (word == "f") {
      int one, two, three;
      ss >> one >> two >> three;
      faces.push_back( Index(one-1, two-1, three-1) );
    }
  }

}

int main(int argc, char** argv)
{
  Phototube::Viewer::readFile(argc, argv);
  Phototube::Viewer::initGlut(argc, argv);
  return 0;
}


