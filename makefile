# makefile for phototube
# Author: Eric Alan Bischoff
# Platform: Mac OS X 10.8.5

TARGET     = phototube
CC         = g++
LD         = g++
DEBUG      = -g
LIBPATH    = -L"/System/Library/Frameworks/OpenGL.framework/Libraries"
LIBRARIES  = -lGL -lGLU -lm -ltcl
FRAMEWORKS = -framework GLUT -framework OpenGL
CFLAGS     = -Wall -c -frounding-math
LFLAGS     = -Wall $(FRAMEWORKS) $(LIBPATH) $(LIBRARIES)
OBJS       = obj/Viewer.o

all: $(TARGET)

$(TARGET): $(OBJS)
	$(LD) $(LFLAGS) $(OBJS) -o $(TARGET)

obj/Viewer.o: src/Viewer.cpp
	$(CC) $(CFLAGS) -o $@ $<

clean:
	rm -f obj/*
	rm -f $(TARGET)

